# {{name}}

{{desc}}


## 💡 Features

- {{feature_list}}

## 📚 Documentation

Documentation for this project is available [here](https://docs.getcryst.al)!

**Support** is also available in our [Discord](https://getcryst.al/discord) and the [Matrix](https://matrix.to/#/#space:getcryst.al). If you face any issues with the software, feel free to open an issue on this repository.


## 👥 Contributors

A list of all **{{name}}** contributors is available in [CONTRIBUTORS.md](CONTRIBUTORS.md)

For a list of **{{name}}** maintainers specifically, see [.gitlab/CODEOWNERS](.gitlab/CODEOWNERS)


## 💾 Installation

### 💽 From Binary

**Install {{name}} using `{{toolkit}}`**
```bash
  $ {{toolkit}} install {{name}}
  $ {{name}}
```

    
### 🏗 From Source

**Install {{name}} from source using `cargo`**
```bash
$ git clone https://git.getcryst.al/{{path}} && cd {{name}}
$ cargo install --path .
```
## 📸 Screenshots

![App Screenshot](https://via.placeholder.com/468x300?text=App+Screenshot+Here)


## 🙌 Contributing

If you'd like to contribute to **{{name}}**, please follow the [Crystal Linux contributing guidelines](https://git.getcryst.al/crystal/info/-/blob/main/CONTRIBUTING.md)!

This project uses `{{toolkit}}`, to set up `{{toolkit}}` for **{{name}}** development, please follow the guidelines below:

{{instructions}}

We are also constantly looking for translators for our i18n-enabled projects! If you speak more than one language, consider helping out on our [Weblate](https://i18n.getcryst.al)!

![https://i18n.getcryst.al/engage/crystal-linux/](https://i18n.getcryst.al/widgets/crystal-linux/-/287x66-black.png)


## 📜 License

[GPLv3-only](https://choosealicense.com/licenses/gpl-3.0/)

![](https://git.getcryst.al/crystal/misc/branding/-/raw/main/banners/README-banner.png)
